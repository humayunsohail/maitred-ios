//
//  MDCatoriesHeaders.h
//  MaitreD
//
//  Created by Humayun on 27/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#ifndef MDCatoriesHeaders_h
#define MDCatoriesHeaders_h

#import "UIColor+Helpers.h"
#import "UIFont+Helpers.h"
#import "UIBarButtonItem+Helpers.h"
#import "UIButton+Helpers.h"
#import "UIImage+Helpers.h"
#import "UISlider+Helpers.h"

#endif /* MDCatoriesHeaders_h */
