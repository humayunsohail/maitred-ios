//
//  UIColor+Helpers.h
//  MaitreD
//
//  Created by Humayun on 27/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Helpers)

+ (UIColor*) applicationColor;
+ (UIColor*) bodyColor;
+ (UIColor*) applicationlightGrayColor;
+ (UIColor*) applicationGrayColor;

@end
