//
//  UIColor+Helpers.m
//  MaitreD
//
//  Created by Humayun on 27/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "UIColor+Helpers.h"

@implementation UIColor (Helpers)

+ (UIColor*) applicationColor{
    static UIColor *applicationColor;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        applicationColor =  [UIColor colorWithRed:242.0/255.0 green:73.0/255.0 blue:2.0/255.0 alpha:1.0];
    });
    
    return applicationColor;
}

+ (UIColor*) bodyColor{
    static UIColor *applicationColor;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        applicationColor =  [UIColor colorWithRed:27.0/255.0 green:27.0/255.0 blue:27.0/255.0 alpha:1.0];
    });
    
    return applicationColor;
}

+ (UIColor*) applicationlightGrayColor{
    static UIColor *applicationlightGrayColor;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        applicationlightGrayColor =  [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0];
    });
    
    return applicationlightGrayColor;
}

+ (UIColor*) applicationGrayColor{
    static UIColor *applicationGrayColor;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        applicationGrayColor =  [UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1.0];
    });
    
    return applicationGrayColor;
}
@end
