//
//  UIFont+Helpers.m
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "UIFont+Helpers.h"

const struct MDFontNames  MDFontNames =
{
    .laoUIBold = @"LaoUI-Bold",
    .laoUIRegular = @"LaoUI"
};

@implementation UIFont (Helpers)

+ (nonnull instancetype)laoUIRegularWithSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:MDFontNames.laoUIRegular size:fontSize];
}

+ (nonnull instancetype)laoUIBoldWithSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:MDFontNames.laoUIBold size:fontSize];
}

@end
