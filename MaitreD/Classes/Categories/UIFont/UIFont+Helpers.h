//
//  UIFont+Helpers.h
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

extern const struct MDFontNames
{
    __unsafe_unretained NSString * __nonnull laoUIBold;
    __unsafe_unretained NSString * __nonnull laoUIRegular;
    
} MDFontNames;

@interface UIFont (Helpers)

+ (nonnull instancetype)laoUIRegularWithSize:(CGFloat)fontSize;
+ (nonnull instancetype)laoUIBoldWithSize:(CGFloat)fontSize;

@end
