//
//  UIImage+Helpers.h
//  MaitreD
//
//  Created by Humayun on 04/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Helpers)

+ (UIImage*)blurified:(UIImage*)theImage;
+ (UIImage *)imageFromLayer:(CALayer *)layer;

@end
