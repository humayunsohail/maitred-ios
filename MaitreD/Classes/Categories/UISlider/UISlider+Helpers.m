//
//  UISlider+Helpers.m
//  MaitreD
//
//  Created by Humayun on 04/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "UISlider+Helpers.h"

@implementation UISlider (Helpers)

- (UISlider *)setSliderGradientBackgroundWithColors:(NSArray *)colors
{
    CAGradientLayer *trackGradientLayer = [CAGradientLayer layer];
    CGRect sliderFrame = self.frame;
    sliderFrame.size.height = 5.0; //set the height of slider
    trackGradientLayer.frame = sliderFrame;
    trackGradientLayer.colors = colors;
    trackGradientLayer.startPoint = CGPointMake(0.0, 0.5);
    trackGradientLayer.endPoint = CGPointMake(1.0, 0.5);
    trackGradientLayer.cornerRadius = 3.0;
    
    UIImage *trackImage = [[UIImage imageFromLayer:trackGradientLayer] resizableImageWithCapInsets:UIEdgeInsetsZero];
    
    [self setMinimumTrackImage:trackImage forState:UIControlStateNormal];
    [self setMaximumTrackImage:trackImage forState:UIControlStateNormal];
    
    return self;
}

@end
