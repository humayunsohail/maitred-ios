//
//  UISlider+Helpers.h
//  MaitreD
//
//  Created by Humayun on 04/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISlider (Helpers)

- (UISlider *)setSliderGradientBackgroundWithColors:(NSArray *)colors;


@end
