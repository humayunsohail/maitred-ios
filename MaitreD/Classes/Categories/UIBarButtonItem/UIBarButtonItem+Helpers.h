//
//  UIBarButtonItem+Helpers.h
//  MaitreD
//
//  Created by Humayun on 03/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (Helpers)

+(NSArray*) barButtons;

+ (UIBarButtonItem*)backArrowButtonWithTarget:(id)target action:(SEL)action;
+ (UIBarButtonItem*)cartButtonWithTarget:(id)target action:(SEL)action image:(UIImage*)image;
+ (UIBarButtonItem*)userButtonWithTarget:(id)target action:(SEL)action image:(UIImage*)image;
+ (UIBarButtonItem*)rightBarButtonWithTitle:(id)target action:(SEL)action andTitle:(NSString*)BtnTitle;

+ (UIBarButtonItem*)rightBarButtonWithTarget:(id)target action:(SEL)action;
+ (UIBarButtonItem*)leftBarButtonWithTitle:(id)target action:(SEL)action andTitle:(NSString*)BtnTitle;

@end
