//
//  UIBarButtonItem+Helpers.m
//  MaitreD
//
//  Created by Humayun on 03/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "UIBarButtonItem+Helpers.h"

@implementation UIBarButtonItem (Helpers)

+(NSArray*) barButtons
{
    UIImage *buttonImage = [UIImage imageNamed:@"search"];
    
    //create the button and assign the image
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    
    //set the frame of the button to the size of the image (see note below)
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    
    //create a UIBarButtonItem with the button as a custom view
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    
    UIImage *buttonImage1 = [UIImage imageNamed:@"search"];
    
    //create the button and assign the image
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button1 setImage:buttonImage1 forState:UIControlStateNormal];
    
    //set the frame of the button to the size of the image (see note below)
    button1.frame = CGRectMake(0, 0, buttonImage1.size.width, buttonImage1.size.height);
    
    //create a UIBarButtonItem with the button as a custom view
    UIBarButtonItem *customBarItem1 = [[UIBarButtonItem alloc] initWithCustomView:button1];
    
    
    NSArray *arr = [[NSArray alloc] initWithObjects:customBarItem,customBarItem1, nil];
    
    return arr;
}

+ (UIBarButtonItem*)cartButtonWithTarget:(id)target action:(SEL)action image:(UIImage*)image
{
    UIImage *buttonImage = image;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    return customBarItem;
}

+ (UIBarButtonItem*)backArrowButtonWithTarget:(id)target action:(SEL)action
{
    UIImage *buttonImage = [UIImage imageNamed:@"backButton"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    return customBarItem;
}

+ (UIBarButtonItem*)userButtonWithTarget:(id)target action:(SEL)action image:(UIImage*)image{
    UIImage *buttonImage = image;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button setTintColor:[UIColor clearColor]];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    button.layer.cornerRadius = button.frame.size.width/2;
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    return customBarItem;
}

+(UIBarButtonItem*)rightBarButtonWithTitle:(id)target action:(SEL)action andTitle:(NSString*)BtnTitle
{
    CGSize textSize = [BtnTitle sizeWithAttributes:@{NSFontAttributeName:[UIFont laoUIRegularWithSize:15.0]}];

    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, textSize.width, textSize.height)];
    
    [btn setTitle:BtnTitle forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor applicationColor] forState:UIControlStateNormal];
    [btn.titleLabel setTextAlignment:NSTextAlignmentLeft];
    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [btn.titleLabel setFont:[UIFont laoUIRegularWithSize:15.0]];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithCustomView:btn];
    return back;
}

///////////////////////////////////////////////////////////////////////////////////
+ (UIBarButtonItem*)rightBarButtonWithTarget:(id)target action:(SEL)action
{
    UIImage *buttonImage = [UIImage imageNamed:@"notification.png"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    return customBarItem;
}

+ (UIBarButtonItem*)leftBarButtonWithTitle:(id)target action:(SEL)action andTitle:(NSString*)BtnTitle
{
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 25)];
    
    [btn setTitle:BtnTitle forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btn.titleLabel setFont:[UIFont laoUIRegularWithSize:15.0]];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithCustomView:btn];
    return back;
    
}
@end
