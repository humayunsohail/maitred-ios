//
//  UIButton+Helpers.h
//  MaitreD
//
//  Created by Humayun on 03/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Helpers)

- (UIButton *)buttonWithTitle:(NSString *)title andWithTitleColor:(UIColor *)titleColor andWithTitleSize:(CGFloat)titleFontSize andWithFrame:(CGRect)frame andWithBackgroundColor:(UIColor *)color andWithCornerRadius:(CGFloat)radius andWithTarget:(id)target andWithAction:(SEL)action;


@end
