//
//  UIButton+Helpers.m
//  MaitreD
//
//  Created by Humayun on 03/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "UIButton+Helpers.h"

@implementation UIButton (Helpers)

- (UIButton *)buttonWithTitle:(NSString *)title andWithTitleColor:(UIColor *)titleColor andWithTitleSize:(CGFloat)titleFontSize andWithFrame:(CGRect)frame andWithBackgroundColor:(UIColor *)color andWithCornerRadius:(CGFloat)radius andWithTarget:(id)target andWithAction:(SEL)action{
    
    [self setTitle:title forState:UIControlStateNormal];
    [self setTitleColor:titleColor forState:UIControlStateNormal];
    [self setTintColor:[UIColor clearColor]];
    [self.titleLabel setFont:[UIFont laoUIRegularWithSize:titleFontSize]];
    [self setBackgroundColor:color];
    [self addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    self.layer.cornerRadius = radius;

    return self;
}

@end
