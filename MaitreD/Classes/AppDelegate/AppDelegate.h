//
//  AppDelegate.h
//  MaitreD
//
//  Created by Humayun on 26/07/2016.
//  Copyright © 2016 Humayun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

