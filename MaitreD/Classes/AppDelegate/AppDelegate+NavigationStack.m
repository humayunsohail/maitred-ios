//
//  AppDelegate+NavigationStack.m
//  MaitreD
//
//  Created by Humayun on 27/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "AppDelegate+NavigationStack.h"

#import "MDIntroLoginViewController.h"
#import "MDSplashViewController.h"
#import "MDRestaurantsViewController.h"

@implementation AppDelegate (NavigationStack)

- (void) initializeRootNavigationStack
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:[[MDSplashViewController alloc] initWithNibName:@"MDSplashViewController" bundle:nil]];
    
    navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    navigationController.navigationBar.tintColor = [UIColor bodyColor];
    
    navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor bodyColor], NSFontAttributeName:[UIFont laoUIRegularWithSize:18.0]};
        
    navigationController.navigationBar.translucent = NO;

    self.window.rootViewController = navigationController;
    
    // Set View and make it Visible
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
}

@end
