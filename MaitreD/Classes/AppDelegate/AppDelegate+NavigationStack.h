//
//  AppDelegate+NavigationStack.h
//  MaitreD
//
//  Created by Humayun on 27/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (NavigationStack)

- (void) initializeRootNavigationStack;

@end
