//
//  MDRestaurantTableViewCell.m
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "MDFoodItemTableViewCell.h"

@implementation MDFoodItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //Label
    self.nameLabel.textColor = [UIColor bodyColor];
    self.typeLabel.textColor = [UIColor applicationGrayColor];
    self.costLabel.textColor = [UIColor applicationColor];
    
    //self.addButton.selected = value from datasource
    
    
    //ImageView
    self.foodImageView.layer.cornerRadius = 5.0;
    
    //Buttons
    [self.addButton buttonWithTitle:@"Add" andWithTitleColor:[UIColor whiteColor] andWithTitleSize:12.0 andWithFrame:self.addButton.frame andWithBackgroundColor:[UIColor applicationGrayColor] andWithCornerRadius:3.0 andWithTarget:self andWithAction:@selector(addButtonTapped:)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Action methods

- (void)addButtonTapped:(id)sender{
    
    self.addButton.selected = !self.addButton.selected;
    
    if(self.didPressAddButtonCallback){
        self.didPressAddButtonCallback(sender, self.addButton.selected);
    }
}

@end
