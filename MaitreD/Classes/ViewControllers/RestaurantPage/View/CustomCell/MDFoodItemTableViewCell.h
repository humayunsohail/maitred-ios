//
//  MDRestaurantTableViewCell.h
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDFoodItemTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *costLabel;

@property (retain, nonatomic) IBOutlet UIButton *addButton;

@property (weak, nonatomic) IBOutlet UIImageView *foodImageView;

@property (nonatomic, copy) void(^didPressAddButtonCallback)(id sender, BOOL isSelected);

@end
