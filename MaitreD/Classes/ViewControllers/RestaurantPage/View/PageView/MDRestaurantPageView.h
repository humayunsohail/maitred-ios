//
//  MDRestaurantPageView.h
//  MaitreD
//
//  Created by Humayun on 03/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MDRestaurantFoodListTableView.h"

@interface MDRestaurantPageView : UIView

@property (weak, nonatomic, nullable) IBOutlet UILabel *restaurantNameLabel;
@property (weak, nonatomic, nullable) IBOutlet UILabel *restaurantDistanceLabel;
@property (weak, nonatomic, nullable) IBOutlet UILabel *restauntItemsCountLabel;

@property (weak, nonatomic, nullable) IBOutlet UITextView *restaurantDescriptionTextview;

@property (retain, nonatomic, nullable) IBOutlet UITableView *restaurantItemsTableView
;

@end
