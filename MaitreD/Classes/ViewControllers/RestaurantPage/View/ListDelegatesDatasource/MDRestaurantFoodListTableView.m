//
//  MDRestaurantFoodListTableView.m
//  MaitreD
//
//  Created by Humayun on 03/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDRestaurantFoodListTableView.h"

@implementation MDRestaurantFoodListTableView

#pragma mark - Init methods

#pragma mark - Tableview delegate/datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MDFoodItemTableViewCellID";
    
    MDFoodItemTableViewCell *cell = (MDFoodItemTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MDFoodItemTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.clipsToBounds = YES;
    cell.layer.masksToBounds = YES;
    
    __weak typeof(cell) weakself = cell;

    cell.didPressAddButtonCallback = ^(id sender, BOOL isSelected){
        if(self.didRecieveAddCallback){
            if(isSelected){
                weakself.addButton.backgroundColor = [UIColor applicationColor];
            }else{
                weakself.addButton.backgroundColor = [UIColor applicationGrayColor];
            }
            self.didRecieveAddCallback(indexPath, isSelected);
        }
    };
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

@end
