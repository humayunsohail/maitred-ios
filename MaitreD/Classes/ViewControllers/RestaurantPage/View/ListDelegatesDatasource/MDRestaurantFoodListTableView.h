//
//  MDRestaurantFoodListTableView.h
//  MaitreD
//
//  Created by Humayun on 03/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MDFoodItemTableViewCell.h"

@interface MDRestaurantFoodListTableView : NSObject<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain) IBOutlet MDFoodItemTableViewCell *foodItemTableViewCell;

@property (nonatomic, copy) void(^didRecieveAddCallback)(NSIndexPath *indexPath, BOOL isSelected);

@end
