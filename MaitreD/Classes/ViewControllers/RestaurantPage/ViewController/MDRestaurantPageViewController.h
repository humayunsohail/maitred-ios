//
//  MDRestaurantPageViewController.h
//  MaitreD
//
//  Created by Humayun on 03/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MDRestaurantPageView;

@interface MDRestaurantPageViewController : UIViewController

@property (nonatomic, weak, nullable, readonly) IBOutlet MDRestaurantPageView *restaurantPageView;

@end
