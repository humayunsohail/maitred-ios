//
//  MDRestaurantPageViewController.m
//  MaitreD
//
//  Created by Humayun on 03/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDRestaurantPageViewController.h"

@interface MDRestaurantPageViewController ()

@property (nonatomic, weak, nullable, readwrite) IBOutlet MDRestaurantPageView *restaurantPageView;

@property (retain, nonatomic, nullable) MDRestaurantFoodListTableView *foodItemsTableView;

@end

@implementation MDRestaurantPageViewController

#pragma mark - Init methods

- (nonnull instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:@"MDRestaurantPageViewController" bundle:nil];
    if (self) {
        self.title = @"Name of the restaurant";
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem backArrowButtonWithTarget:self action:@selector(viewDidPop)];
        self.navigationItem.rightBarButtonItem = [UIBarButtonItem cartButtonWithTarget:self action:@selector(cartButtonTapped) image:[UIImage imageNamed:@"cartEmpty"]];
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.foodItemsTableView = [[MDRestaurantFoodListTableView alloc] init];
        
    self.restaurantPageView.restaurantItemsTableView.delegate = self.foodItemsTableView;
    self.restaurantPageView.restaurantItemsTableView.dataSource = self.foodItemsTableView;
    
    [self configureCallbacks];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
}

- (void)viewDidPop{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Private methods

- (void)configureCallbacks {
    __weak typeof(self) weakself = self;
    
    weakself.foodItemsTableView.didRecieveAddCallback = ^(NSIndexPath *indexPath, BOOL isSelected){
        if(isSelected){
            NSLog(@"Item Selected");
        }else{
            NSLog(@"Item Unselected");
        }
    };
}

#pragma mark - Action methods

- (void)cartButtonTapped{
    MDCartViewController *cartViewController = [[MDCartViewController alloc] initWithNibName:@"MDCartViewController" bundle:nil];
    [self.navigationController pushViewController:cartViewController animated:YES];
}

@end
