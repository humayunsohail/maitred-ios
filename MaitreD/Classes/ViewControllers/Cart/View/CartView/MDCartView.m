//
//  MDCartView.m
//  MaitreD
//
//  Created by Humayun on 03/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDCartView.h"

@implementation MDCartView

#pragma mark - Init methods

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}

- (nonnull instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

#pragma mark - Action methods

- (IBAction)segmentControlValueChanged:(UISegmentedControl *)sender{
    if(self.didChangeSegmentControlCallback){
        self.didChangeSegmentControlCallback(sender.selectedSegmentIndex);
    }
}
@end
