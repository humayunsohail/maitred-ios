//
//  MDCartView.h
//  MaitreD
//
//  Created by Humayun on 03/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDCartView : UIView

@property (weak, nonatomic, nullable) IBOutlet UITextField *tableNumberLabel;

@property (weak, nonatomic, nullable) IBOutlet UISegmentedControl *segmentControl;

@property (weak, nonatomic, nullable) IBOutlet UITableView *orderTableView;

@property (weak, nonatomic, nullable) IBOutlet UIView *tableNumberView;

@property (retain, nonatomic, nullable) IBOutlet NSLayoutConstraint *tableViewYConstraint;

@property (copy, nonatomic, nullable) void(^didChangeSegmentControlCallback)(NSInteger index);


@end
