//
//  MDRestaurantTableViewCell.m
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDDiscountVoucherTableViewCell.h"

@implementation MDDiscountVoucherTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.codeTextField.textColor = [UIColor bodyColor];
        
    [self.redeemButton buttonWithTitle:@"Redeem" andWithTitleColor:[UIColor whiteColor] andWithTitleSize:13.0 andWithFrame:self.redeemButton.frame andWithBackgroundColor:[UIColor applicationColor] andWithCornerRadius:5.0 andWithTarget:self andWithAction:@selector(redeemButtonTapped:)];
    
    self.backgroundColor = [UIColor applicationlightGrayColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Action Methods

- (void)redeemButtonTapped:(id)sender{
}

@end
