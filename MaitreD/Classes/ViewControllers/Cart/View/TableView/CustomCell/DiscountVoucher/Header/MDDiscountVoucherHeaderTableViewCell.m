//
//  MDRestaurantTableViewCell.m
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDDiscountVoucherHeaderTableViewCell.h"

@implementation MDDiscountVoucherHeaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //Labels
    self.discountVoucherLabel.textColor = [UIColor bodyColor];

    self.backgroundColor = [UIColor applicationlightGrayColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Action methods

- (IBAction)voucherAddMinusButtonTapped:(id)sender{
    self.voucherAddMinusButton.selected = !self.voucherAddMinusButton.selected;
    
    if(self.didPressAddMinusButtonCallback){
        self.didPressAddMinusButtonCallback();
    }
}
@end
