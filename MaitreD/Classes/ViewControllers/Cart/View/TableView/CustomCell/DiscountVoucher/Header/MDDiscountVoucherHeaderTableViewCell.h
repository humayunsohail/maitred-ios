//
//  MDRestaurantTableViewCell.h
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDDiscountVoucherHeaderTableViewCell: UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *discountVoucherLabel;

@property (weak, nonatomic) IBOutlet UIButton *voucherAddMinusButton;

@property (weak, nonatomic) IBOutlet UIImageView *voucherAddMinusImageView;

@property (nonatomic, copy) void(^didPressAddMinusButtonCallback)();

@end
