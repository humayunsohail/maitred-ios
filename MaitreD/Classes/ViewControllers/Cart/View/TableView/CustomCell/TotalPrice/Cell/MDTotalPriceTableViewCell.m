//
//  MDRestaurantTableViewCell.m
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDTotalPriceTableViewCell.h"

@implementation MDTotalPriceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.subtotalLabel.textColor = [UIColor applicationGrayColor];
    self.costSubtotalLabel.textColor = [UIColor applicationGrayColor];
    self.taxesLabel.textColor = [UIColor applicationGrayColor];
    self.costTaxesLabel.textColor = [UIColor applicationGrayColor];
    self.totalLabel.textColor = [UIColor bodyColor];
    self.costTotalLabel.textColor = [UIColor bodyColor];
    
    [self.placeOrderButton buttonWithTitle:@"PLACE ORDER" andWithTitleColor:[UIColor whiteColor] andWithTitleSize:15.0 andWithFrame:self.placeOrderButton.frame andWithBackgroundColor:[UIColor applicationColor] andWithCornerRadius:5.0 andWithTarget:self andWithAction:@selector(placeOrderButtonTapped:)];

}

#pragma mark - Action methods

- (void)placeOrderButtonTapped:(id)sender{
    if(self.didPressPlaceOrderCallback){
        self.didPressPlaceOrderCallback();
    }
}
@end
