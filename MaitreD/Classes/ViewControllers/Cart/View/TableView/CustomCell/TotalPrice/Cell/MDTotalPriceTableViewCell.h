//
//  MDRestaurantTableViewCell.h
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDTotalPriceTableViewCell: UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *subtotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *taxesLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *costSubtotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *costTaxesLabel;
@property (weak, nonatomic) IBOutlet UILabel *costTotalLabel;

@property (weak, nonatomic) IBOutlet UIButton *placeOrderButton;


@property (nonatomic, copy) void(^didPressPlaceOrderCallback)();


@end
