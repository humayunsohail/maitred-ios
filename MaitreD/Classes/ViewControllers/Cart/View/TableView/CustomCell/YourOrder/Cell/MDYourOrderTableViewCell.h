//
//  MDRestaurantTableViewCell.h
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDYourOrderTableViewCell: UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *productsLabel;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end
