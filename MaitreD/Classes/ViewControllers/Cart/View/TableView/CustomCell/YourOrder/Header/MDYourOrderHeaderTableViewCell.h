//
//  MDRestaurantTableViewCell.h
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDYourOrderHeaderTableViewCell: UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *yourOrderLabel;
@property (weak, nonatomic) IBOutlet UILabel *productsLabel;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (weak, nonatomic) IBOutlet UIView *headerView;

@end
