//
//  MDRestaurantTableViewCell.m
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDYourOrderTableViewCell.h"

@implementation MDYourOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.productsLabel.textColor = [UIColor bodyColor];
    self.quantityLabel.textColor = [UIColor bodyColor];
    self.priceLabel.textColor = [UIColor bodyColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
