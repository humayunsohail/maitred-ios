//
//  MDRestaurantTableViewCell.m
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDYourOrderHeaderTableViewCell.h"

@implementation MDYourOrderHeaderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //Labels
    self.yourOrderLabel.textColor = [UIColor bodyColor];
    self.productsLabel.textColor = [UIColor bodyColor];
    self.quantityLabel.textColor = [UIColor bodyColor];
    self.priceLabel.textColor = [UIColor bodyColor];
    
    //Header
    self.headerView.backgroundColor = [UIColor applicationlightGrayColor];
    self.headerView.layer.cornerRadius = 3.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
