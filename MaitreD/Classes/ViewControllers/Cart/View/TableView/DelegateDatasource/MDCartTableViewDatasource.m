//
//  MDCartTableViewDatasource.m
//  MaitreD
//
//  Created by Humayun on 03/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDCartTableViewDatasource.h"

@interface MDCartTableViewDatasource ()

@end

@implementation MDCartTableViewDatasource

#pragma mark - Tableview delegate/datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0){
        return 3;
    }else if(section == 1){
        if(self.shouldExpandDiscountVoucher){
            return 1;
        }else{
            return 0;
        }
    }else{
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;

    if(section == 0){
        return 35.0;
    }else if(section == 1){
        return 120.0;
    }else{
        return 170.0;
    }}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0){
        return 60.0;
    }else if(section == 1){
        return 30.0;
    }else{
        return 0.0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    
    if(section == 0){
        UITableViewCell *cell = [self tableView:tableView cellForYourOrderRowAtIndexPath:indexPath];
        return cell;
    }else if(section == 1){
        UITableViewCell *cell = [self tableView:tableView cellForDiscountVoucherRowAtIndexPath:indexPath];
        return cell;
    }else{
        UITableViewCell *cell = [self tableView:tableView cellForTotalPriceRowAtIndexPath:indexPath];
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    if(section == 0){
        UIView *cell = [self tableView:tableView viewForYourOrderHeaderInSection:section];
        return cell;
    }else if(section == 1){
        UIView *cell = [self tableView:tableView viewForDiscountVoucherHeaderInSection:section];
        return cell;
    }else{
        return nil;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark - Your Order 

- (UITableViewCell *)tableView:(UITableView *)tableView cellForYourOrderRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"MDYourOrderTableViewCell";
    
    MDYourOrderTableViewCell *cell = (MDYourOrderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MDYourOrderTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForYourOrderHeaderInSection:(NSInteger)section{
    static NSString *cellIdentifier = @"MDYourOrderHeaderTableViewCell";

    MDYourOrderHeaderTableViewCell *cell = (MDYourOrderHeaderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MDYourOrderHeaderTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    return cell;
}

#pragma mark - Discount Voucher

- (UITableViewCell *)tableView:(UITableView *)tableView cellForDiscountVoucherRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"MDDiscountVoucherTableViewCell";
    
    MDDiscountVoucherTableViewCell *cell = (MDDiscountVoucherTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MDDiscountVoucherTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForDiscountVoucherHeaderInSection:(NSInteger)section{
    static NSString *cellIdentifier = @"MDDiscountVoucherHeaderTableViewCell";
    
    MDDiscountVoucherHeaderTableViewCell *cell = (MDDiscountVoucherHeaderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MDDiscountVoucherHeaderTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    cell.didPressAddMinusButtonCallback = ^(){
        if(self.didRecieveAddMinusCallback){
            self.didRecieveAddMinusCallback();
        }
    };

    if(self.shouldExpandDiscountVoucher){
        cell.voucherAddMinusImageView.image = [UIImage imageNamed:@"subtractVoucher"];
    }else{
        cell.voucherAddMinusImageView.image = [UIImage imageNamed:@"addVoucher"];
    }
    
    return cell;
}

#pragma mark - Total Price

- (UITableViewCell *)tableView:(UITableView *)tableView cellForTotalPriceRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"MDTotalPriceTableViewCell";
    
    MDTotalPriceTableViewCell *cell = (MDTotalPriceTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MDTotalPriceTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    __weak typeof(cell) weakself = cell;
    weakself.didPressPlaceOrderCallback = ^(){
        if(self.didRecievePlaceOrderCallback){
            self.didRecievePlaceOrderCallback();
        }
    };

    return cell;
}


@end
