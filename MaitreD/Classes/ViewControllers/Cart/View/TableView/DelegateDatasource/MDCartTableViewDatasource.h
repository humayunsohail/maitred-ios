//
//  MDCartTableViewDatasource.h
//  MaitreD
//
//  Created by Humayun on 03/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MDYourOrderHeaderTableViewCell.h"
#import "MDYourOrderTableViewCell.h"
#import "MDDiscountVoucherHeaderTableViewCell.h"
#import "MDDiscountVoucherTableViewCell.h"
#import "MDTotalPriceTableViewCell.h"

@interface MDCartTableViewDatasource : NSObject<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, assign) BOOL shouldExpandDiscountVoucher;

@property (nonatomic, retain) IBOutlet MDYourOrderHeaderTableViewCell *yourOrderHeaderTableViewCell;
@property (nonatomic, retain) IBOutlet MDYourOrderTableViewCell *yourOrderTableViewCell;

@property (nonatomic, retain) IBOutlet MDDiscountVoucherHeaderTableViewCell *discountVoucherHeaderTableViewCell;
@property (nonatomic, retain) IBOutlet MDDiscountVoucherTableViewCell *discountVoucherTableViewCell;

@property (nonatomic, retain) IBOutlet MDTotalPriceTableViewCell *totalPriceTableViewCell;

@property (nonatomic, copy) void(^didRecieveAddMinusCallback)();
@property (nonatomic, copy) void(^didRecievePlaceOrderCallback)();

@end
