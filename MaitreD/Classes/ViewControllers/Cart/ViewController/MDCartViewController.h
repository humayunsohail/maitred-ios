//
//  MDCartViewController.h
//  MaitreD
//
//  Created by Humayun on 03/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MDCartView;

@interface MDCartViewController : UIViewController

@property (nonatomic, weak, nullable, readonly) IBOutlet MDCartView *cartView;

@end
