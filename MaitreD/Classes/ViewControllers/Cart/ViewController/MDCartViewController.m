//
//  MDCartViewController.m
//  MaitreD
//
//  Created by Humayun on 03/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDCartViewController.h"

#import "MDFeedbackViewController.h"

@interface MDCartViewController ()

@property (nonatomic, weak, nullable, readwrite) MDCartView *cartView;

@property (retain, nonatomic, nullable) MDCartTableViewDatasource *orderTableView;

@end

@implementation MDCartViewController

#pragma mark - Init methods

- (nonnull instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:@"MDCartViewController" bundle:nil];
    if (self) {
        self.title = @"Your Order";
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem backArrowButtonWithTarget:self action:@selector(viewDidPop)];
    }
    return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
    [self configureCallbacks];
}

- (void)viewDidPop{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Private methods

- (void)configureView{
    self.cartView.tableNumberLabel.textColor = [UIColor bodyColor];
    
    self.orderTableView = [[MDCartTableViewDatasource alloc] init];
    self.orderTableView.shouldExpandDiscountVoucher = true;
    
    self.cartView.orderTableView.delegate = self.orderTableView;
    self.cartView.orderTableView.dataSource = self.orderTableView;

    self.cartView.segmentControl.tintColor = [UIColor applicationColor];
}

- (void)configureCallbacks{
    __weak typeof(self) weakself = self;

    weakself.cartView.didChangeSegmentControlCallback = ^(NSInteger segmentControlIndex){
        [self handleSegmentControlState:segmentControlIndex];
    };
    
    weakself.orderTableView.didRecieveAddMinusCallback = ^(){
        [self handleTableViewState];
    };
    
    weakself.orderTableView.didRecievePlaceOrderCallback = ^(){
        [self handlePlaceOrderEvent];
    };
}

#pragma mark - Segment Control

- (void)handleSegmentControlState:(NSInteger)index{
    switch (index){
        case 0:
            [self handleTableNumberViewState:NO];
            break;
        case 1:
            [self handleTableNumberViewState:YES];
            break;
        default:
            break;
    }
}

- (void)handleTableNumberViewState:(BOOL)state{
    if(state){
        self.cartView.tableViewYConstraint.constant = self.cartView.tableViewYConstraint.constant - (self.cartView.tableNumberView.frame.size.height/1.5);
    }else{
        self.cartView.tableViewYConstraint.constant = self.cartView.tableViewYConstraint.constant + (self.cartView.tableNumberView.frame.size.height/1.5);
    }
    self.cartView.tableNumberView.hidden = state;
    
    [UIView transitionWithView:self.cartView.orderTableView duration: 0.2f options: UIViewAnimationOptionCurveEaseInOut animations: ^(void){
        [self.view layoutIfNeeded];
    } completion: ^(BOOL finished){
    }];
}

#pragma mark - Tableview

- (void)handleTableViewState{
    self.orderTableView.shouldExpandDiscountVoucher = !self.orderTableView.shouldExpandDiscountVoucher;

    [UIView transitionWithView:self.cartView.orderTableView duration: 0.2f options: UIViewAnimationOptionCurveEaseInOut animations: ^(void){
        CGPoint offset = self.cartView.orderTableView .contentOffset;
        [self.cartView.orderTableView reloadData];
        [self.cartView.orderTableView setContentOffset:offset];
    } completion: ^(BOOL finished){
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - Place Order

- (void)handlePlaceOrderEvent{
    MDFeedbackViewController *feedbackViewController = [[MDFeedbackViewController alloc] initWithNibName:@"MDFeedbackViewController" bundle:nil];
    
    [self.navigationController pushViewController:feedbackViewController animated:YES];
}
@end
