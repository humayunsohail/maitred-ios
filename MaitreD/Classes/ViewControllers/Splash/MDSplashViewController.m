//
//  MDSplashViewController.m
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDSplashViewController.h"

#import "MDIntroLoginViewController.h"

#import "YLImageView.h"
#import "YLGIFImage.h"

@interface MDSplashViewController ()

@property (weak, nonatomic) IBOutlet YLImageView *splashGIFView;

@property (retain, nonatomic) NSTimer *timer;

@property (assign, nonatomic) float seconds;

@end

@implementation MDSplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    
    self.splashGIFView.image = [YLGIFImage imageNamed:@"Splash.gif"];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(countSeconds) userInfo:nil repeats:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
}
- (void)countSeconds
{
    self.seconds = self.seconds + 0.5;
        
    if(self.seconds == 2.5)
    {
        [self.timer invalidate];
        
        MDIntroLoginViewController *loginViewController = [[MDIntroLoginViewController alloc] initWithNibName:@"MDIntroLoginViewController" bundle:nil];
        
        [self.navigationController pushViewController:loginViewController animated:YES];
    }
}

@end
