//
//  MDIntroLoginViewController.h
//  MaitreD
//
//  Created by Humayun on 26/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MDIntroLoginView;

@interface MDIntroLoginViewController : UIViewController

@property (nonatomic, weak, nullable, readonly) IBOutlet MDIntroLoginView *introLoginView;

@end
