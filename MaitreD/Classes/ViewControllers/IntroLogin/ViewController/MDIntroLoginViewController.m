//
//  MDIntroLoginViewController.m
//  MaitreD
//
//  Created by Humayun on 26/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//
#import "SwipeView.h"

#import "MDIntroLoginViewController.h"
#import "MDIntroLoginView.h"
#import "MDIntrosHandler.h"

@interface MDIntroLoginViewController ()<UIScrollViewDelegate>

@property (nonatomic, weak, nullable, readwrite) IBOutlet MDIntroLoginView *introLoginView;
@property (nonatomic, strong, nonnull) MDIntrosHandler *handler;

@end

@implementation MDIntroLoginViewController

#pragma mark - Init methods

- (nonnull instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:@"MDIntroLoginViewController" bundle:nil];
    if (self) {
        self.navigationController.navigationBarHidden = YES;
    }
    return self;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    [self configureView];
    [self configureDatasource];
    [self configureCallbacks];
    
    [self.handler load];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:NO];
    self.navigationController.navigationBarHidden = YES;
}

#pragma mark - Property methods

- (MDIntrosHandler *)handler {
    if (!_handler) {
        _handler = [[MDIntrosHandler alloc] initWithViewController:self];
    }
    return _handler;
}

#pragma mark - Private methods

- (void)configureView{
    self.introLoginView.introPageControl.currentPageIndicatorTintColor = [UIColor applicationColor];
    self.introLoginView.introPageControl.transform = CGAffineTransformMakeScale(1.5, 1.5);
    
    self.introLoginView.introHeaderLabel.textColor = [UIColor bodyColor];
    self.introLoginView.introbodyTextview.textColor = [UIColor bodyColor];
    
    self.introLoginView.introScrollView.delegate = self;
}

- (void)configureCallbacks {
      self.introLoginView.didPressFacebookSignInCallback = ^() {
          [self loginWithFacebook];
    };
}

#pragma mark - Datasource methods

- (void)configureDatasource{
    self.introLoginView.introsList = [MDIntrosHandler getIntrosDatasourceObjectsList];
    
    [self changeMultiplier:self.introLoginView.contentViewWidthConstraint to:self.introLoginView.introsList.list.count];
    
    NSDictionary *introObject = (NSDictionary *)[self.introLoginView.introsList.list objectAtIndex:0];
    self.introLoginView.introHeaderLabel.text = introObject[MDIntroDatasourceObjectKeys.header];
    self.introLoginView.introbodyTextview.text = introObject[MDIntroDatasourceObjectKeys.body];
}

#pragma mark - Layout methods

- (void)changeMultiplier:(NSLayoutConstraint *)constraint to:(CGFloat)newMultiplier{
    NSLayoutConstraint *newConstraint = [NSLayoutConstraint constraintWithItem:constraint.firstItem attribute:constraint.firstAttribute relatedBy:constraint.relation toItem:constraint.secondItem attribute:constraint.secondAttribute multiplier:newMultiplier constant:constraint.constant];
    
    [self.introLoginView removeConstraint:constraint];
    [self.introLoginView addConstraint:newConstraint];
    constraint = newConstraint;
}

#pragma mark - ScrollView

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    
    self.introLoginView.introPageControl.currentPage = page;
    
    NSDictionary *introObject = (NSDictionary *)[self.introLoginView.introsList.list objectAtIndex:page];
    
    self.introLoginView.introHeaderLabel.text = introObject[MDIntroDatasourceObjectKeys.header];
    self.introLoginView.introbodyTextview.text = introObject[MDIntroDatasourceObjectKeys.body];
    
    self.introLoginView.introChildView.frame = CGRectMake(page * self.introLoginView.introChildView.frame.size.width, self.introLoginView.introChildView.frame.origin.y, self.introLoginView.introChildView.frame.size.width, self.introLoginView.introChildView.frame.size.height);
    
    [self.introLoginView layoutIfNeeded];
}


#pragma mark - Facebook Login

- (void)loginWithFacebook{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    [login logInWithReadPermissions: @[@"email", @"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error){
        
        if (error){
            NSLog(@"Process error");
        }
        else if (result.isCancelled){
            NSLog(@"Cancelled");
        }
        else{
            MDRestaurantsViewController *restaurantsViewController = [[MDRestaurantsViewController alloc] initWithNibName:@"MDRestaurantsViewController" bundle:nil];
             
            [self.navigationController pushViewController:restaurantsViewController animated:YES];
        }
    }];
}

@end
