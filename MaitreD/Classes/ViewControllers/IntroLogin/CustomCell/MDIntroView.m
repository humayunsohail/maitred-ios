//
//  MDIntroView.m
//  MaitreD
//
//  Created by Humayun on 02/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDIntroView.h"

#import "MDIntroViewCell.h"

@implementation MDIntroView

#pragma mark - iCarousel methods

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    //return the total number of items in the carousel
    return self.datasourceArray.count;
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    static NSString *cellIdentifier = @"MDIntroViewID";

    MDIntroViewCell *tutorialView = [[MDIntroViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    if (tutorialView == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MDIntroViewCell" owner:self options:nil];
        tutorialView = [nib objectAtIndex:0];
    }

    return tutorialView;
}


@end
