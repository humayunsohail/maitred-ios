//
//  MDRestaurantTableViewCell.m
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDIntroViewCell.h"

@implementation MDIntroViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //Text colors
    self.headerLabel.textColor = [UIColor bodyColor];
    self.bodyTextview.textColor = [UIColor bodyColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
