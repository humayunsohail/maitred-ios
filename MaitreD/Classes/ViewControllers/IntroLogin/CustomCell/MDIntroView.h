//
//  MDIntroView.h
//  MaitreD
//
//  Created by Humayun on 02/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SwipeView.h"

@interface MDIntroView : NSObject<SwipeViewDataSource, SwipeViewDelegate>

@property (nonatomic, retain) NSArray *datasourceArray;

@end
