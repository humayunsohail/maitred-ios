//
//  MDIntroLoginView.m
//  MaitreD
//
//  Created by Humayun on 02/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDIntroLoginView.h"

@interface MDIntroLoginView () <UIScrollViewDelegate>

@end

@implementation MDIntroLoginView

#pragma mark - Init methods

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}

- (nonnull instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

#pragma mark - Action Methods

- (IBAction)loginWithFacebookTapped:(id)sender{
    if (self.didPressFacebookSignInCallback) {
        self.didPressFacebookSignInCallback();
    }
}

@end
