//
//  MDIntroLoginView.h
//  MaitreD
//
//  Created by Humayun on 02/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MDIntrosDatasourceList.h"
#import "MDIntroDatasourceObject.h"

#import "SwipeView.h"

@interface MDIntroLoginView : UIView

#pragma mark - IBOutlets
@property (weak, nonatomic, nullable) IBOutlet UILabel *introHeaderLabel;

@property (weak, nonatomic, nullable) IBOutlet UITextView *introbodyTextview;

@property (weak, nonatomic, nullable) IBOutlet UIView *introParentView;
@property (weak, nonatomic, nullable) IBOutlet UIView *introContentView;
@property (weak, nonatomic, nullable) IBOutlet UIView *introChildView;

@property (weak, nonatomic, nullable) IBOutlet UIScrollView *introScrollView;

@property (weak, nonatomic, nullable) IBOutlet SwipeView *tutorialSwipeView;

@property (weak, nonatomic, nullable) IBOutlet UIPageControl *introPageControl;

@property (retain, nonatomic, nullable) IBOutlet NSLayoutConstraint *contentViewWidthConstraint;

@property (retain, nonatomic, nullable, readwrite) MDIntrosDatasourceList *introsList;

#pragma mark - Callbacks

@property (nonatomic, copy, nullable) void(^didPressFacebookSignInCallback)();

@end
