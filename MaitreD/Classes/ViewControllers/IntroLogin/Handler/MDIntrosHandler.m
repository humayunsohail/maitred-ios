//
//  MDIntrosHandler.m
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//
#import "MDIntrosHandler.h"

#import "MDIntroLoginView.h"
#import "MDIntroLoginViewController.h"

@interface MDIntrosHandler ()

@property (nonatomic, weak, nullable) MDIntroLoginView *view;
@property (nonatomic, weak, nullable) MDIntroLoginViewController *introViewController;

@end

@implementation MDIntrosHandler

#pragma mark - Init methods

- (nonnull instancetype)initWithViewController:(nonnull MDIntroLoginViewController *)viewController {
    self = [super init];
    if (self) {
        self.introViewController = viewController;
        self.view = self.introViewController.introLoginView;
    }
    return self;
}

#pragma mark - BIHandlerBase methods

- (void)load {
    [super load];
}

#pragma mark - Intro Datasource

+ (NSString *)getIntroPlistPath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:kIntroDatasourceFilename];
    
    NSError *error;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath])
    {
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"MDIntroDatasource" ofType:@"plist"];
        
        [[NSFileManager defaultManager] copyItemAtPath:bundle toPath:plistPath error:&error];
    }
    
    return plistPath;
}

+ (NSDictionary *)getIntrosDictionary{
    NSString *plistPath = [self getIntroPlistPath];
    
    return [[NSDictionary alloc] initWithContentsOfFile:plistPath];
}

+ (MDIntrosDatasourceList *)getIntrosDatasourceObjectsList{
    NSDictionary *introsDictionary = [self getIntrosDictionary];
    
    if(introsDictionary != nil)
    {
        NSError *error;
        MDIntrosDatasourceList *introsList = [MTLJSONAdapter modelOfClass:MDIntrosDatasourceList.class fromJSONDictionary:introsDictionary error:&error];
        
        return introsList;
    }
    return nil;
}

@end
