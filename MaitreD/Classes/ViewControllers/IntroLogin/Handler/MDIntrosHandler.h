//
//  MDIntrosHandler.h
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BIObjCHelpers/NSString+BIExtra.h>

#import "BIHandlerBase.h"

#import "MDIntrosDatasourceList.h"

@class MDIntroLoginViewController;

@interface MDIntrosHandler : BIHandlerBase

- (nonnull instancetype)initWithViewController:(nonnull MDIntroLoginViewController *)view;

#pragma mark - Static Class Methods

+ (nonnull NSDictionary *)getIntrosDictionary;

+ (nonnull MDIntrosDatasourceList *)getIntrosDatasourceObjectsList;

@end
