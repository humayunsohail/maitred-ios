//
//  MDProfileView.m
//  MaitreD
//
//  Created by Humayun on 04/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDProfileView.h"

@implementation MDProfileView

#pragma mark - Init methods

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}

- (nonnull instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

#pragma mark - Action methods

- (IBAction)distanceSliderValueChanged:(UISlider *)sender{
    if(self.didChangeDistanceSliderCallback){
        self.didChangeDistanceSliderCallback(sender.value);
    }
}
@end
