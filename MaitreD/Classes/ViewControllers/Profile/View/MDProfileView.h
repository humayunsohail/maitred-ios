//
//  MDProfileView.h
//  MaitreD
//
//  Created by Humayun on 04/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDProfileView : UIView

@property (weak, nonatomic, nullable) IBOutlet UILabel *distanceValue;

@property (weak, nonatomic, nullable) IBOutlet UIImageView *profilePictureImageView;
@property (weak, nonatomic, nullable) IBOutlet UIImageView *coverPictureImageView;

@property (weak, nonatomic, nullable) IBOutlet UISlider *distanceSlider;

@property (nonatomic, copy, nullable) void(^didChangeDistanceSliderCallback)(float value);

@end
