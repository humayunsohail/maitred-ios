//
//  MDProfileViewController.h
//  MaitreD
//
//  Created by Humayun on 04/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MDProfileView;

@interface MDProfileViewController : UIViewController

@property (nonatomic, weak, nullable, readonly) IBOutlet MDProfileView *profileView;

@end
