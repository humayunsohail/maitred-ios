//
//  MDProfileViewController.m
//  MaitreD
//
//  Created by Humayun on 04/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDProfileViewController.h"

#import "MDProfileView.h"

@interface MDProfileViewController ()

@property (nonatomic, weak, nullable, readwrite) MDProfileView *profileView;

@end

@implementation MDProfileViewController

#pragma mark - Init methods

- (nonnull instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:@"MDProfileViewController" bundle:nil];
    if (self) {
        self.title = @"Profile";
        
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem backArrowButtonWithTarget:self action:@selector(viewDidPop)];
        self.navigationItem.rightBarButtonItem = [UIBarButtonItem rightBarButtonWithTitle:self action:@selector(logout) andTitle:@"Logout"];
    }
    return self;
}

#pragma mark - View lifecyle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
    [self configureCallbacks];
}

- (void)viewDidPop{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Logout

- (void)logout{
    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:1] animated:YES];
}

#pragma mark - Private methods

- (void)configureView{
    //Profile Picture
    self.profileView.profilePictureImageView.layer.cornerRadius = self.profileView.profilePictureImageView.frame.size.width/2;
    
    //Cover Picture
    [self blurifyCoverImage];
    
    //Slider
    self.profileView.distanceSlider.thumbTintColor = [UIColor applicationColor];
    NSArray *colors = [NSArray arrayWithObjects:(id)[UIColor applicationColor].CGColor, (id)[UIColor whiteColor].CGColor, nil];
    [self.profileView.distanceSlider setSliderGradientBackgroundWithColors:colors];
    
    self.profileView.distanceValue.text = [NSString stringWithFormat:@"%d KM", (int)self.profileView.distanceSlider.value];
}

- (void)blurifyCoverImage{
    __weak __typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        __typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            UIImage *blurifiedImage = [UIImage blurified:self.profileView.coverPictureImageView.image];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.profileView.coverPictureImageView.image = blurifiedImage;
            });
        }
    });
}

- (void)configureCallbacks{
    self.profileView.didChangeDistanceSliderCallback = ^(float value){
        self.profileView.distanceValue.text = [NSString stringWithFormat:@"%d KM", (int)value];
    };
}

@end
