//
//  MDFeedbackView.m
//  MaitreD
//
//  Created by Humayun on 04/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDFeedbackView.h"

@implementation MDFeedbackView

#pragma mark - Init methods

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}

- (nonnull instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

#pragma mark - Feedback

- (IBAction)feedbackButtonTapped:(UIButton *)sender{
    if(self.didChangeFeedbackBarCallback){
        self.didChangeFeedbackBarCallback((NSInteger)sender.tag);
    }
}

@end
