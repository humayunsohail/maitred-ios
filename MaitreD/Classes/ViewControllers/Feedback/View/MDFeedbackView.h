//
//  MDFeedbackView.h
//  MaitreD
//
//  Created by Humayun on 04/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDFeedbackView : UIView

@property (weak, nonatomic, nullable) IBOutlet UILabel *codeNumberLabel;
@property (weak, nonatomic, nullable) IBOutlet UILabel *codeHeaderLabel;
@property (weak, nonatomic, nullable) IBOutlet UILabel *howDidWeDoLabel;

@property (weak, nonatomic, nullable) IBOutlet UITextView *howDidWeDoTextview;

@property (weak, nonatomic, nullable) IBOutlet UITextField *reviewTextfield;

@property (weak, nonatomic, nullable) IBOutlet UIButton *submitButton;

@property (copy, nonatomic, nullable) void(^didChangeFeedbackBarCallback)(NSInteger tag);

@end
