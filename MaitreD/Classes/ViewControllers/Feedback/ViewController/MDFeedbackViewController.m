//
//  MDFeedbackViewController.m
//  MaitreD
//
//  Created by Humayun on 04/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDFeedbackViewController.h"

#import "MDFeedbackView.h"

@interface MDFeedbackViewController ()

@property (nonatomic, weak, nullable, readwrite) MDFeedbackView *feedbackView;

@end

@implementation MDFeedbackViewController

#pragma mark - Init methods

- (nonnull instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:@"MDFeedbackViewController" bundle:nil];
    if (self) {
        self.title = @"Feedback";
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem rightBarButtonWithTitle:self action:nil andTitle:@""];

        self.navigationItem.rightBarButtonItem = [UIBarButtonItem rightBarButtonWithTitle:self action:@selector(doneFeedback) andTitle:@"Done"];
    }
    return self;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
    [self configureCallbacks];
}

#pragma mark - Private methods

- (void)configureView{
    self.feedbackView.codeHeaderLabel.textColor = [UIColor bodyColor];
    self.feedbackView.codeNumberLabel.textColor = [UIColor applicationColor];
    self.feedbackView.howDidWeDoLabel.textColor = [UIColor bodyColor];

    self.feedbackView.howDidWeDoTextview.textColor = [UIColor bodyColor];

    self.feedbackView.reviewTextfield.textColor = [UIColor bodyColor];
    
    [self.feedbackView.submitButton buttonWithTitle:@"SUBMIT" andWithTitleColor:[UIColor whiteColor] andWithTitleSize:15.0 andWithFrame:self.feedbackView.submitButton.frame andWithBackgroundColor:[UIColor applicationColor] andWithCornerRadius:5.0 andWithTarget:self andWithAction:@selector(submitButtonTapped:)];
}

- (void)configureCallbacks{
    __weak typeof(self) weakself = self;
    
    weakself.feedbackView.didChangeFeedbackBarCallback = ^(NSInteger tag){
        [self handleRating:tag];
    };
}

#pragma mark - Rating 

- (void)handleRating:(NSInteger)tag{
    for(NSInteger i=1;i<=5;i++){
        UIButton *ratingButton = (UIButton *)[self.feedbackView viewWithTag:i];
        [ratingButton setImage:[UIImage imageNamed:@"ratingUnselected"] forState:UIControlStateNormal];
    }
    for(NSInteger i=1;i<=tag;i++){
        UIButton *ratingButton = (UIButton *)[self.feedbackView viewWithTag:i];
        [ratingButton setImage:[UIImage imageNamed:@"ratingSelected"] forState:UIControlStateNormal];
    }
}


#pragma mark - Submit

- (void)submitButtonTapped:(id)sender{
    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:2] animated:YES];
}

#pragma mark - Done

- (void)doneFeedback{
    [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:2] animated:YES];
}
@end
