//
//  MDFeedbackViewController.h
//  MaitreD
//
//  Created by Humayun on 04/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MDFeedbackView;

@interface MDFeedbackViewController : UIViewController

@property (nonatomic, weak, nullable, readonly) IBOutlet MDFeedbackView *feedbackView;


@end
