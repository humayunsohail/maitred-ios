//
//  MDRestaurantsViewController.m
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDRestaurantsViewController.h"

#import "MDProfileViewController.h"

@interface MDRestaurantsViewController ()

@property (retain, nonatomic, readwrite) NSMutableDictionary *facebookDictionary;

@end

@implementation MDRestaurantsViewController

#pragma mark - Init methods

- (nonnull instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:@"MDRestaurantsViewController" bundle:nil];
    if (self) {
        self.title = @"List Of Restaurants";
        
        self.navigationItem.rightBarButtonItem = [UIBarButtonItem userButtonWithTarget:self action:@selector(userButtonTapped:) image:[UIImage imageNamed:@"userPlaceholder"]];
    }
    return self;
}

#pragma mark - View Lifecyle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Navigation Controller
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    
    //Table View
    self.restaurantsTableViewDatasource = [[MDRestaurantTableView alloc] init];
    
    self.restaurantsTableView.estimatedRowHeight = 200;
    self.restaurantsTableView.rowHeight = UITableViewAutomaticDimension;
    
    self.restaurantsTableView.delegate = self.restaurantsTableViewDatasource;
    self.restaurantsTableView.dataSource = self.restaurantsTableViewDatasource;
    
    //Call backs
    [self configureCallbacks];
    
    
}

#pragma mark - Private methods

- (void)configureCallbacks {
    __weak typeof(self) weakself = self;

    weakself.restaurantsTableViewDatasource.didPressRestaurantCallback = ^(NSIndexPath *indexPath) {

        MDRestaurantPageViewController *restaurantPageViewController = [[MDRestaurantPageViewController alloc] initWithNibName:@"MDRestaurantPageViewController" bundle:nil];
        
        [weakself.navigationController pushViewController:restaurantPageViewController animated:YES];
    };
}

#pragma mark - Profile

- (void)userButtonTapped:(id)sender{
    MDProfileViewController *profileViewController = [[MDProfileViewController alloc] initWithNibName:@"MDProfileViewController" bundle:nil];
    
    [self.navigationController pushViewController:profileViewController animated:YES];
}

@end
