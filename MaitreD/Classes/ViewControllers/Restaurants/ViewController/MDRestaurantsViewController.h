//
//  MDRestaurantsViewController.h
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MDRestaurantTableView.h"

@interface MDRestaurantsViewController : UIViewController

#pragma mark - IBOutlets

@property (weak, nonatomic) IBOutlet UITableView *restaurantsTableView;

#pragma mark - Instances

@property (retain, nonatomic) MDRestaurantTableView *restaurantsTableViewDatasource;

@end
