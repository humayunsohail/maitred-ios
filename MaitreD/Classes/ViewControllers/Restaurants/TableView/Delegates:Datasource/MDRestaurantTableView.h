//
//  MDRestaurantTableView.h
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MDRestaurantTableViewCell.h"

@interface MDRestaurantTableView : NSObject <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain) IBOutlet MDRestaurantTableViewCell *restaurantTableViewCell;

@property (nonatomic, copy) void(^didPressRestaurantCallback)(NSIndexPath *indexPath);

@end
