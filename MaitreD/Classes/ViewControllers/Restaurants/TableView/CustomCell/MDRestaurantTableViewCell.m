//
//  MDRestaurantTableViewCell.m
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDRestaurantTableViewCell.h"

@implementation MDRestaurantTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //Text colors
    self.nameLabel.textColor = [UIColor bodyColor];
    self.descriptionLabel.textColor = [UIColor lightGrayColor];
    self.distanceValueLabel.textColor = [UIColor applicationColor];
    self.distanceUnitLabel.textColor = [UIColor applicationColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
