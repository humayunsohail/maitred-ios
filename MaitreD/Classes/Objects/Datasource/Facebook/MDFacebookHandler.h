//
//  MDFacebookHandler.h
//  MaitreD
//
//  Created by Humayun on 04/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXTERN const struct MDFacebookUserDataKeys{
    __unsafe_unretained NSString * __nonnull name;
    __unsafe_unretained NSString * __nonnull profilePhotoURL;
    __unsafe_unretained NSString * __nonnull coverPhotoURL;
} MDFacebookUserDataKeys;

@interface MDFacebookHandler : NSObject

+ (NSMutableDictionary *)getLoggedInUserData;

@end
