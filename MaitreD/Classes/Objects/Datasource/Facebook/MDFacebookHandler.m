//
//  MDFacebookHandler.m
//  MaitreD
//
//  Created by Humayun on 04/08/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDFacebookHandler.h"

const struct MDFacebookUserDataKeys MDFacebookUserDataKeys = {
    .profilePhotoURL = @"ProfilePictureURL",
    .name = @"ProfileName"
};

@implementation MDFacebookHandler

+ (NSMutableDictionary *)getLoggedInUserData{
    NSMutableDictionary *userData = [[NSMutableDictionary alloc] init];
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             
             NSDictionary *resultDictionary = (NSDictionary *)result;
             
             [userData setValue:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large", resultDictionary[@"id"]] forKey:MDFacebookUserDataKeys.profilePhotoURL];
             [userData setValue:resultDictionary[@"name"] forKey:MDFacebookUserDataKeys.name];

         }
     }];
    
    return userData;
}

@end
