//
//  MDIntrosDatasourceList.h
//  MaitreD
//
//  Created by Humayun on 27/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <Mantle/Mantle.h>

FOUNDATION_EXTERN const struct MDIntrosDatasourceListKeys{
    __unsafe_unretained NSString * __nonnull list;
} MDIntrosDatasourceListKeys;

@interface MDIntrosDatasourceList : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy, nullable, readonly) NSArray *list;

@end
