//
//  MDIntroDatasourceObject.h
//  MaitreD
//
//  Created by Humayun on 27/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <Mantle/Mantle.h>

FOUNDATION_EXTERN const struct MDIntroDatasourceObjectKeys {
    __unsafe_unretained NSString * __nonnull header;
    __unsafe_unretained NSString * __nonnull body;
} MDIntroDatasourceObjectKeys;

@interface MDIntroDatasourceObject : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy, nonnull, readonly) NSString *header;
@property (nonatomic, copy, nonnull, readonly) NSString *body;


@end
