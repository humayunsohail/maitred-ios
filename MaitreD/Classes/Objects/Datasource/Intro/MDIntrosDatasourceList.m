//
//  MDIntrosDatasourceList.m
//  MaitreD
//
//  Created by Humayun on 27/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDIntrosDatasourceList.h"

#import "MDIntroDatasourceObject.h"

const struct MDIntrosDatasourceListKeys MDIntrosDatasourceListKeys = {
    .list = @"Intros"
};

@interface MDIntrosDatasourceList ()

@property (nonatomic, copy, nullable, readwrite) NSArray *list;

@end

@implementation MDIntrosDatasourceList

+ (nonnull NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{ @"list": MDIntrosDatasourceListKeys.list
              };
}

+ (NSValueTransformer *)IntrosJSONTransformer{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[MDIntroDatasourceObject class]];
}

@end
