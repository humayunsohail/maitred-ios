//
//  MDIntroDatasourceObject.m
//  MaitreD
//
//  Created by Humayun on 27/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDIntroDatasourceObject.h"

const struct MDIntroDatasourceObjectKeys MDIntroDatasourceObjectKeys = {
    .header = @"header",
    .body = @"body"
};


@interface MDIntroDatasourceObject ()

@property (nonatomic, copy, nullable, readwrite) NSString *header;
@property (nonatomic, copy, nullable, readwrite) NSString *body;

@end

@implementation MDIntroDatasourceObject

+ (nonnull NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"header": MDIntroDatasourceObjectKeys.header,
             @"body": MDIntroDatasourceObjectKeys.body
            };
}

@end
