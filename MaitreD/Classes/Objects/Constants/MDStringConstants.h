//
//  MDStringConstants.h
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MDStringConstants : NSObject

#pragma mark - Filenames

extern NSString * const kIntroDatasourceFilename;

@end
