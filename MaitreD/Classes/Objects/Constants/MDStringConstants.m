//
//  MDStringConstants.m
//  MaitreD
//
//  Created by Humayun on 29/07/2016.
//  Copyright © 2016 Bin. All rights reserved.
//

#import "MDStringConstants.h"

@implementation MDStringConstants

#pragma mark - Filenames

NSString * const kIntroDatasourceFilename = @"MDIntroDatasource.plist";

@end
