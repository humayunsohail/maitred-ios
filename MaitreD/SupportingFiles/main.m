//
//  main.m
//  MaitreD
//
//  Created by Humayun on 26/07/2016.
//  Copyright © 2016 Humayun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
